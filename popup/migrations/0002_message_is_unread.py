# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('popup', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='message',
            name='is_unread',
            field=models.BooleanField(default=True, verbose_name='Is unread'),
        ),
    ]
