# -*- coding: utf-8 -*-


from django.db import models
from django.conf import settings
from django.utils.translation import ugettext_lazy as _


__author__ = 'Peter Perepelyatnik (infdods@yandex.ru)'


# Раз в ExtJS мы подключили локаль, пусть уж и тут у нас все будет переведено
class Message(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=_('User'))
    text = models.TextField(_('Text'), max_length=255)
    timestamp = models.DateTimeField(_('Timestamp'), auto_now_add=True)
    is_unread = models.BooleanField(_('Is unread'), default=True)

    class Meta:
        verbose_name = _('Message')
        verbose_name_plural = _('Messages')
        ordering = ('-timestamp', )

    def __unicode__(self):
        return self.text[:50]
