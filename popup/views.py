# -*- coding: utf-8 -*-


from rest_framework import generics
from popup.models import Message
from popup.serializers import MessageSerializer


__author__ = 'Peter Perepelyatnik (infdods@yandex.ru)'


class MessageApiListView(generics.ListAPIView):
    serializer_class = MessageSerializer

    def get_queryset(self):
        amount = self.request.GET.get('amount')
        if amount:
            return Message.objects.all()[:int(amount)]
        return Message.objects.filter(is_unread=True).all()

    def list(self, *args, **kwargs):
        """
        Юзеру будем выдавать только то что не прочитано
        """
        response = super(MessageApiListView, self).list(*args, **kwargs)
        for message in self.get_queryset():
            message.is_unread = False
            message.save(update_fields=['is_unread'])
        return response
