from django.conf.urls import url
from django.views.generic import TemplateView
from popup import views


urlpatterns = [
    url(r'^$', TemplateView.as_view(template_name='index.html'), name='index'),
    url(r'^get-messages/$', views.MessageApiListView.as_view(), name='get-messages'),
]
