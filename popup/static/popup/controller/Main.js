Ext.define('popup.controller.Main', {
    extend: 'Ext.app.Controller',
    requires: [
        'popup.view.PopupWindow'
    ],
    init: function() {
        var me = this;

        this.control({
            '#messages_button': {

                click: function(btn) {
                    me.checkMessages(0, Ext.getCmp('comments_amount').getValue())
                },

                afterrender: function(btn) {
                    // раз обходимся без сокетов, просто будем чекать по таймеру
                    Ext.TaskManager.start({
                        run: me.checkMessages,
                        interval: 5000
                    });
                }
            },

            '#comments_amount': {
                change: function( cls, newValue, oldValue, eOpts ) {
                    Ext.getCmp('messages_button').setText('Show me last ' + newValue +' comments!!!')
                }
            }
        });
    },

    checkMessages: function(counter, messagesAmount) {
        var params = {};
        // Если передался параметр, значит мы кликнули на кнопочку и передали количество нужных сообщений
        if (typeof(messagesAmount) != 'undefined') {
            params = {amount: messagesAmount}
        }
        Ext.Ajax.request({
            url: '/popup/get-messages/',
            timeout: 60000,
            method: 'GET',
            params: params,
            scope: this,
            success: function(response, opts) {
                var resp = Ext.decode(response.responseText);
                for (var i=0, len=resp.length; i<len; i++) {
                    popup.view.PopupWindow.show_popup(resp[i].author, resp[i].text);
                }
            },
            failure: function(response, opts){
                console.error(response);
            }
        });
    }
});
