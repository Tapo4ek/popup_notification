Ext.define('popup.view.PopupWindow', {
    requires:[
        "popup.view.packages.Notification"
    ],
    statics: {
        show_popup: function(title, html) {
            var nt = Ext.widget('uxNotification',
                {
                    title: title,
                    position: 'br',
                    manager: 'demo2',
                    spacing: 20,
                    autoClose: false,
                    html: html
                }
            );
            nt.show();
        }
    }
});