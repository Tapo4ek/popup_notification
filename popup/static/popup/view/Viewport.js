Ext.define('popup.view.Viewport', {
    extend : 'Ext.container.Viewport',
    alias  : 'widget.mainViewport',

    layout : 'fit',

    items : [{
        xtype : 'form',
        title : 'Приложение загружено',
        layout: 'hbox',
        bodyPadding : 10,
        items : [
            {
                xtype : 'button',
                text : 'Show me last 1 comments!!!',
                id: 'messages_button'
            },
            {
                xtype: 'numberfield',
                id: 'comments_amount',
                margin: '0 0 0 10',
                editable: 'false',
                value: 1,
                maxValue: 99,
                minValue: 1
            }
        ]
    }]

});