# -*- coding: utf-8 -*-


from django.contrib import admin
from popup.models import Message


__author__ = 'Peter Perepelyatnik (infdods@yandex.ru)'


@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    list_display = ('user', 'text', 'timestamp', 'is_unread')
    readonly_fields = ('timestamp',)
