# -*- coding: utf-8 -*-


from rest_framework import serializers
from popup.models import Message


__author__ = 'Peter Perepelyatnik (infdods@yandex.ru)'


class MessageSerializer(serializers.ModelSerializer):

    author = serializers.SerializerMethodField()

    class Meta:
        model = Message
        fields = ('text', 'author')

    def get_author(self, obj):
        """
        Если у меня есть имя и фамилия пользователя, то в поле автора впишу их, если нет, то username
        """
        if obj.user.first_name and obj.user.last_name:
            return '{0} {1}.'.format(obj.user.first_name, obj.user.last_name[:1])
        return obj.user.username