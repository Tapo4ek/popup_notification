# -*- coding: utf-8 -*-


import os


__author__ = 'Peter Perepelyatnik (infdods@yandex.ru)'


DEBUG = False
BASE_DIR = os.path.dirname(os.path.dirname(__file__))
STATIC_ROOT = os.path.join(BASE_DIR, "static")

REST_FRAMEWORK = {
    'DEFAULT_RENDERER_CLASSES': (
        'rest_framework.renderers.JSONRenderer',
    )
}