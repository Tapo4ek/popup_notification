# -*- coding: utf-8 -*-


from django.conf import settings


__author__ = 'Peter Perepelyatnik (infdods@yandex.ru)'


def global_settings(request):
    return {
        'extjs': settings.EXTJS  # эта переменная нам нужна глобально (на случай если у нас много apps)
    }
